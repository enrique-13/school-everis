package pe.com.everis.main;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.fasterxml.jackson.databind.ObjectMapper;

import pe.com.everis.main.models.entity.Student;
import pe.com.everis.main.restcontroller.StudentRest;
import pe.com.everis.main.services.implement.StudentImplement;

@RunWith(SpringRunner.class)
@WebMvcTest(StudentRest.class)
class StudentRestUnitTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private StudentImplement studentServ;


	//Test Student
	private Student studentTest = new Student(1L, "M", "Carol", "Wuhan", "Pedraza", "Student");

	@Test
	public void testGetAllStudentsSuccess() throws Exception {
		List<Student> users = Arrays.asList(
				studentTest);
	    when(studentServ.findAll()).thenReturn(users);
	    mockMvc.perform(get("/api/students/"))
	            .andExpect(status().isOk());
	}

	public static String asJsonString(final Object obj) {
	    try {
	        final ObjectMapper mapper = new ObjectMapper();
	        final String jsonContent = mapper.writeValueAsString(obj);
	        return jsonContent;
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}

	@Test
	public void testSaveStudentSuccces() throws Exception {
		Student student = studentTest;
		when(studentServ.save(student)).thenReturn(student);
	    mockMvc.perform(
	            post("/api/students")
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(student)))
	            .andExpect(status().isCreated());
	}

	@Test
	public void testSaveStudentFail() throws Exception {
		Student student = new Student("MRR", "Carol", "Wuhan", "Pedraza",new Date(), "Student");
		when(studentServ.save(student)).thenReturn(student);
	    mockMvc.perform(
	            post("/api/students")
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(student)));
	}


	@Test
	public void testUpdateStudentSuccess() throws Exception {
		Student student =  new Student(2L,"M", "Carol", "Wuhan", "Pedraza", "Student");
		when(studentServ.update(student)).thenReturn(student);
	    mockMvc.perform(
	            put("/api/students/{id}",2L)
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(student)))
	            .andExpect(status().isOk());
	}


	@Test
	public void testUpdateStudentFailNotFound() throws Exception {
		Student student =  new Student(100L,"M", "Carol", "Wuhan", "Pedraza", "Student");
		when(studentServ.update(student)).thenReturn(student);
	    mockMvc.perform(
	            put("/api/students/{id}",100L)
	                    .contentType(MediaType.APPLICATION_JSON)
	                    .content(asJsonString(student)))
	            .andExpect(status().isNotFound());
	}


	@Test
	public void testDeleteUserSuccess() throws Exception {
	    mockMvc.perform(
	            delete("/api/students/{id}",2L))
	            .andExpect(status().isOk());
	}

	@Test
	public void testDeleteUserNotFound() throws Exception {
	    mockMvc.perform(
	            delete("/api/students/{id}",100L))
	            .andExpect(status().isNotFound());
	}
	
	
}
