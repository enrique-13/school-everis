package pe.com.everis.main.swagger;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

	public static final Contact DEFAULT_CONTACT = new Contact("Enrique Encarnacion Nuñez", 
			"https://gitlab.com/ENRIKE13", "enrikefen20@gmail.com");
	
	@SuppressWarnings("rawtypes")
	public static final ApiInfo DEFAULT_API_INFO = new ApiInfo("Everis School", "`Everis School App` 📚  es un proyecto correspondiente al Ejercicio 1 del Bootcamp Everis.", 
			"2020-02-13", "PREMIUN", DEFAULT_CONTACT, "Apache 8.5", "https://www.apache.org/licenses/LICENSE-2.0", 
			new ArrayList<VendorExtension>());
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(DEFAULT_API_INFO);
	}
	
	
}