package pe.com.everis.main.restcontroller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.everis.main.models.entity.Family;
import pe.com.everis.main.services.FamilyService;

@RestController
@RequestMapping("/api/families")
public class FamilyRest {

  @Autowired
  FamilyService familyServ;
  
  /**
   * &#64;Get all the families  Javadoc tag.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener todas las familias",
      notes = "Obtiene la información de las familias")
  public ResponseEntity<List<Family>> fetchFamily() {
    try {
      List<Family> families = familyServ.findAll();
      return new ResponseEntity<List<Family>>(families, HttpStatus.OK);   
    } catch (Exception e) {
      return new ResponseEntity<List<Family>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }

  }

  /**
   * &#64;Get family By Id Member  Javadoc tag.
   */
  @GetMapping(path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener una familia",
      notes = "Ingresar un IdFamily válido")
  public ResponseEntity<Family> fetchFamilyById(@PathVariable("id") Long id) {
    try {
      Optional<Family> family = familyServ.findById(id);
      return new ResponseEntity<Family>(family.get(), HttpStatus.OK);   
    } catch (Exception e) {
      return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
    }

  }
	
  /**
   * &#64;Save family Javadoc tag.
   */
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
       produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para almacenar una familia",
      notes = "Almacena la información de una familia")
  public ResponseEntity<Family> saveFamily(@RequestBody Family family) {
    try {
      Family newFamily = familyServ.save(family);
      return new ResponseEntity<Family>(newFamily, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<Family>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
	
  /**
   * &#64;Update family Javadoc tag.
   */
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para actualizar una familia",
      notes = "Ingresar un IdFamily válido")
  public ResponseEntity<Family> updateFamily(@PathVariable("id") Long id,
      @RequestBody Family family) {
    try {
      Optional<Family> optional = familyServ.findById(id);
      if (optional.isPresent()) {
        family.setFamilyId(id);
        Family updateFamily = familyServ.update(family);
        return new ResponseEntity<Family>(updateFamily, 
          HttpStatus.OK);
      } else {
        return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
      }

    } catch (Exception e) {
      return new ResponseEntity<Family>(HttpStatus.INTERNAL_SERVER_ERROR); 
    }
  }

  /**
   * &#64;Delete family Javadoc tag.
   */
  @DeleteMapping(path = "/{id}")
  @ApiOperation(value = "Servicio para eliminar una familia",
      notes = "Ingresar un IdFamily válido")
  @ApiResponses(value = {
      @ApiResponse(code = 500, message = "Internal Server Error Verifique su objeto JSON"
      + " o si existe alguna dependencia."),
      @ApiResponse(code = 200, message = "OK Se eliminó correctamente la familia.") })
  public ResponseEntity<Family> deleteFamily(@PathVariable("id") Long id) {
    try {
      Optional<Family> optional = familyServ.findById(id);
      if (optional.isPresent()) {
        familyServ.deleteById(id);
        return new ResponseEntity<Family>(HttpStatus.OK);
      } else {
        return new ResponseEntity<Family>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<Family>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
