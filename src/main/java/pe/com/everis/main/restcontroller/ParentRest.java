package pe.com.everis.main.restcontroller;

import java.util.List;
import java.util.Optional;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.everis.main.models.entity.Parent;
import pe.com.everis.main.services.ParentService;

@RestController
@RequestMapping("/api/parents")
public class ParentRest {

  @Autowired
  ParentService parentServ;

  /**
   * &#64;Get All the parents Javadoc tag.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener todos los padres",
          notes = "Obtiene la información de los padres de familia")
  public ResponseEntity<List<Parent>> fetchParent() {
    try {
      List<Parent> parents = parentServ.findAll();
      return new ResponseEntity<List<Parent>>(parents, HttpStatus.OK);   
    } catch (Exception e) {
      return new ResponseEntity<List<Parent>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Get  the parent By Id  Javadoc tag.
   */
  @GetMapping(path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener un padre de familia",
          notes = "Obtiene la información de un padre de familia")
  public ResponseEntity<Parent> fetchParentById(@PathVariable Long id) {
    try {
      Optional<Parent> parent = parentServ.findById(id);
      return new ResponseEntity<Parent>(parent.get(), HttpStatus.OK);   
    } catch (Exception e) {
      return new ResponseEntity<Parent>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * &#64;Save the parents Javadoc tag.
   */
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para almacenar un padre de familia",
          notes = "Almacena la información de un padre de familia")
  public ResponseEntity<Parent> saveParent(@RequestBody Parent parent) {
    try {
      Parent newParent = parentServ.save(parent);
      return new ResponseEntity<Parent>(newParent, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<Parent>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Update the parents Javadoc tag.
  */
  @ApiOperation(value = "Servicio para actualizar a un padre de familia",
          notes = "Ingresar un idParent válido")
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Parent> updateParent(@PathVariable("id") Long id, 
      @RequestBody Parent parent) {
    try {
      Optional<Parent> optional = parentServ.findById(id);
      if (optional.isPresent()) {
        parent.setParentId(id);
        Parent updateParent = parentServ.update(parent);
        return new ResponseEntity<Parent>(updateParent, 
        HttpStatus.OK);
      } else {
        return new ResponseEntity<Parent>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<Parent>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Delete the parents Javadoc tag.
  */
  @DeleteMapping(path = "/{id}")
  @ApiOperation(value = "Servicio para eliminar un padre de familia",
          notes = "Ingresar un idParent válido")
  @ApiResponses(value = {
      @ApiResponse(code = 500, message = "Verifique su objeto JSON o si existe alguna dependencia"),
      @ApiResponse(code = 200, message = "OK Se eliminó correctamente el padre de familia.") })
  public ResponseEntity<Parent> deleteParent(@PathVariable("id") Long id) {
    try {
      Optional<Parent> optional = parentServ.findById(id);
      if (optional.isPresent()) {
        parentServ.deleteById(id);
        return new ResponseEntity<Parent>(HttpStatus.OK);
      } else {
        return new ResponseEntity<Parent>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<Parent>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
