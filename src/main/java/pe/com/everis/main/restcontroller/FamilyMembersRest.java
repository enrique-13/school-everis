package pe.com.everis.main.restcontroller;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pe.com.everis.main.models.entity.FamilyMember;
import pe.com.everis.main.services.FamilyMemberService;

@RestController
@RequestMapping("/api/familymembers")
public class FamilyMembersRest {

  @Autowired
  private FamilyMemberService familyMemberServ;

  /**
   * &#64;Get all the families Javadoc tag.
   */
  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener todos los miembros de todas las familias",
      notes = "Obtiene la información de los miembros de todas la familias")
  public ResponseEntity<List<FamilyMember>> fetchFamilyMember() {
    try {
      List<FamilyMember> families = familyMemberServ.findAll();
      return new ResponseEntity<List<FamilyMember>>(families, HttpStatus.OK);   
    } catch (Exception e) {
      return new ResponseEntity<List<FamilyMember>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Get family member By Id Javadoc tag.
   */
  @GetMapping(path = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener un miembro de una familia",
      notes = "Obtiene la información del miembro de una Familia")
  public ResponseEntity<FamilyMember> fetchFamilyMemberById(@PathVariable("id") Long id) {
    try {
      Optional<FamilyMember> family = familyMemberServ.findById(id);
      return new ResponseEntity<FamilyMember>(family.get(), HttpStatus.OK);   
    } catch (Exception e) {
      return new ResponseEntity<FamilyMember>(HttpStatus.NOT_FOUND);
    }
  }

  /**
   * &#64;Save entity Javadoc tag.
   */
  @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para guardar un miembros de una familia")
  public ResponseEntity<FamilyMember> saveFamilyMember(@RequestBody FamilyMember family) {
    try {
      FamilyMember newFamilyMember = familyMemberServ.save(family);
      return new ResponseEntity<FamilyMember>(newFamilyMember, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<FamilyMember>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Get entity By Id Javadoc tag.
   */
  @GetMapping(path = "/family/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para obtener todos los miembros de una familia",
      notes = "Obtiene la información de los miembros")
  public ResponseEntity<List<FamilyMember>> fetchFamilyMembersByIdFamily(
      @PathVariable("id")Long idFamily) {
    try {
      List<FamilyMember> families = familyMemberServ.fetchFamilyMembersByIdFamily(idFamily);
      return new ResponseEntity<List<FamilyMember>>(families, HttpStatus.OK);  
    } catch (Exception e) {
      return new ResponseEntity<List<FamilyMember>>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Update entity  Javadoc tag.
   */
  @PutMapping(path = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE, 
      produces = MediaType.APPLICATION_JSON_VALUE)
  @ApiOperation(value = "Servicio para actualizar un miembro de una familia")
  public ResponseEntity<FamilyMember> updateFamilyMember(@PathVariable("id") Long id, 
      @RequestBody FamilyMember family) {
    try {
      Optional<FamilyMember> optional = familyMemberServ.findById(id);
      if (optional.isPresent()) {
        family.setFamilyMemberId(id);
        FamilyMember updateFamilyMember = familyMemberServ.update(family);
        return new ResponseEntity<FamilyMember>(updateFamilyMember, 
        HttpStatus.OK);
      } else {
        return new ResponseEntity<FamilyMember>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<FamilyMember>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * &#64;Delete family Member  Javadoc tag.
   */
  @DeleteMapping(path = "/{id}")
  @ApiOperation(value = "Servicio para eliminar un miembro de una familia")
  public ResponseEntity<FamilyMember> deleteFamily(@PathVariable("id") Long id) {
    try {
      Optional<FamilyMember> optional = familyMemberServ.findById(id);
      if (optional.isPresent()) {
        familyMemberServ.deleteById(id);
        return new ResponseEntity<FamilyMember>(HttpStatus.OK);
      } else {
        return new ResponseEntity<FamilyMember>(HttpStatus.NOT_FOUND);
      }
    } catch (Exception e) {
      return new ResponseEntity<FamilyMember>(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }

}
