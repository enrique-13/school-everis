package pe.com.everis.main.models.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pe.com.everis.main.models.entity.Family;
import pe.com.everis.main.models.entity.Student;

@Repository
public interface FamilyRepository extends JpaRepository<Family, Long> {

}
