package pe.com.everis.main.services.implement;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.everis.main.models.entity.FamilyMember;
import pe.com.everis.main.models.repository.FamilyMemberRepository;
import pe.com.everis.main.services.FamilyMemberService;


@Service
public class FamilyMemberImplement implements FamilyMemberService {

	@Autowired
	private FamilyMemberRepository family;
	
	@Transactional(readOnly = true)
	@Override
	public List<FamilyMember> findAll() throws Exception {
		// TODO Auto-generated method stub
		return family.findAll();
	}

	@Override
	public Optional<FamilyMember> findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return family.findById(id);
	}

	@Transactional
	@Override
	public FamilyMember save(FamilyMember entity) throws Exception {
		// TODO Auto-generated method stub
		return family.save(entity);
	}

	@Override
	public FamilyMember update(FamilyMember entity) throws Exception {
		// TODO Auto-generated method stub
		return family.save(entity);
	}

	@Override
	public void deleteById(Long id) throws Exception {
		// TODO Auto-generated method stub
		family.deleteById(id);
	}

	@Override
	public void deleteAll() throws Exception {
		// TODO Auto-generated method stub
		family.deleteAll();
	}

	@Transactional(readOnly = true)
	@Override
	public List<FamilyMember> fetchFamilyMembersByIdFamily(Long idFamily) {
		// TODO Auto-generated method stub
		return family.fetchFamilyMembersByIdFamily(idFamily);
	}

}
