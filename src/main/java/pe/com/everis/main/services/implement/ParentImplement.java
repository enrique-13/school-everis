package pe.com.everis.main.services.implement;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pe.com.everis.main.models.entity.Parent;
import pe.com.everis.main.models.repository.ParentRepository;
import pe.com.everis.main.services.ParentService;

@Service
public class ParentImplement implements ParentService  {

	@Autowired
	private ParentRepository parent;
	
	@Transactional(readOnly = true)
	@Override
	public List<Parent> findAll() throws Exception {
		// TODO Auto-generated method stub
		return parent.findAll();
	}

	@Transactional(readOnly = true)
	@Override
	public Optional<Parent> findById(Long id) throws Exception {
		// TODO Auto-generated method stub
		return parent.findById(id);
	}

	@Transactional
	@Override
	public Parent save(Parent entity) throws Exception {
		// TODO Auto-generated method stub
		return parent.save(entity);
	}

	@Transactional
	@Override
	public Parent update(Parent entity) throws Exception {
		// TODO Auto-generated method stub
		return parent.save(entity);
	}

	@Transactional
	@Override
	public void deleteById(Long id) throws Exception {
		// TODO Auto-generated method stub
		parent.deleteById(id);
	}

	@Transactional
	@Override
	public void deleteAll() throws Exception {
		// TODO Auto-generated method stub
		parent.deleteAll();
	}

}
